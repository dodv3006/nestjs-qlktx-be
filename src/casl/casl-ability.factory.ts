import {
  Ability,
  AbilityBuilder,
  AbilityClass,
  ExtractSubjectType,
  InferSubjects,
} from '@casl/ability';
import { Injectable } from '@nestjs/common';
import { UserEntity } from 'src/auth/entities/user.entity';
import { EAction, ERole } from 'src/common/constants/global.constant';
import { StudentEntity } from 'src/student/entities/student.entity';
type Subjects = InferSubjects<typeof UserEntity | typeof StudentEntity> | 'all';
export type AppAbility = Ability<[EAction, Subjects]>;
interface IPolicyHandler {
  handle(ability: AppAbility): boolean;
}

type PolicyHandlerCallback = (ability: AppAbility) => boolean;

export type PolicyHandler = IPolicyHandler | PolicyHandlerCallback;
@Injectable()
export class CaslAbilityFactory {
  createForUser(user: UserEntity) {
    const { can, cannot, build } = new AbilityBuilder<
      Ability<[EAction, Subjects]>
    >(Ability as AbilityClass<AppAbility>);

    if (user.role === ERole.ADMIN) {
      can(EAction.Manage, 'all');
    } else {
      can(EAction.Update, StudentEntity, ['address', 'dateOfBirth', 'name'], {
        studentID: user.student.studentID,
      });
    }

    return build({
      detectSubjectType: (item) =>
        item.constructor as ExtractSubjectType<Subjects>,
    });
  }
}
