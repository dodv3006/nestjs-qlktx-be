import { Exclude, Expose } from 'class-transformer';
import { IsIn, IsInt, IsPositive, IsString } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { ERoomType } from '../types';
@Exclude()
export class CreateRoomDTO {
  @Expose()
  @IsString()
  @ApiProperty()
  name: string;

  @Expose()
  @IsIn(Object.values(ERoomType))
  @ApiProperty({ enum: Object.values(ERoomType) })
  roomType: ERoomType;

  @Expose()
  @IsPositive()
  @ApiProperty()
  price: number;

  @Expose()
  @IsPositive()
  @IsInt()
  @ApiProperty()
  maxPeople: number;

  constructor(partial: Partial<CreateRoomDTO>) {
    Object.assign(this, partial);
  }
}
