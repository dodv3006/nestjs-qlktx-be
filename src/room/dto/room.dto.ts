import { Exclude, Expose, Transform, Type } from 'class-transformer';
import { IsString } from 'class-validator';
import { StudentDTO } from 'src/student/dto';
import { ERoomType } from '../types';
@Exclude()
export class RoomDTO {
  @Expose()
  readonly id: number;

  @Expose()
  @IsString()
  readonly name: string;

  @Expose()
  readonly roomType: ERoomType;
  @Expose()
  price: number;

  @Expose()
  maxPeople: number;

  @Expose()
  students: StudentDTO[];

  @Expose()
  get totalStudents(): number {
    return this.students.length;
  }

  constructor(partial: Partial<RoomDTO>) {
    Object.assign(this, partial);
  }
}
