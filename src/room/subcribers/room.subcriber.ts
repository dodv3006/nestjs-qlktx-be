import { InjectConnection } from '@nestjs/typeorm';
import {
  Connection,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  Like,
  UpdateEvent,
} from 'typeorm';
import { RoomEntity } from '../entities/room.entity';

@EventSubscriber()
export class RoomSubscriber implements EntitySubscriberInterface<RoomEntity> {
  constructor(@InjectConnection() readonly connection: Connection) {
    connection.subscribers.push(this);
  }

  listenTo() {
    return RoomEntity;
  }

  /**
   * Called before insert.
   */
  async beforeInsert(event: InsertEvent<RoomEntity>) {}

  async beforeUpdate(event: UpdateEvent<RoomEntity>) {}
}
