import { Injectable } from '@nestjs/common';
import { BaseService } from 'src/common/services/BaseService';
import { RoomEntity } from '../entities/room.entity';
import { RoomRepository } from '../repositories/room.repository';

@Injectable()
export class RoomService extends BaseService<RoomEntity, RoomRepository> {
  constructor(repository: RoomRepository) {
    super(repository);
  }
}
