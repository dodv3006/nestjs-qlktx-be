import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { ManualSerialize } from 'src/common/interceptors/serialize.interceptor';
import { CreateRoomDTO } from '../dto/create-room.dto';

import { RoomDTO } from '../dto/room.dto';
import { RoomService } from '../services/room.service';

@ApiTags('Room')
@Controller('room')
@UseGuards(JwtAuthGuard)
export class RoomController {
  constructor(private readonly roomService: RoomService) {}

  @Get('get-all-rooms')
  @ManualSerialize(RoomDTO)
  getAllRooms() {
    return this.roomService.getAll();
  }

  @Post('create-room')
  @ApiBody({ type: [CreateRoomDTO] })
  create(@Body() createRoom: CreateRoomDTO) {
    return this.roomService.create(createRoom);
  }
}
