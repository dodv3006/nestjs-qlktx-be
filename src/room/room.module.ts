import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoomController } from './controller/room.controller';
import { RoomEntity } from './entities/room.entity';
import { RoomRepository } from './repositories/room.repository';
import { RoomService } from './services/room.service';

@Module({
  imports: [TypeOrmModule.forFeature([RoomEntity, RoomRepository])],
  controllers: [RoomController],
  providers: [RoomService],
  exports: [TypeOrmModule, RoomService],
})
export class RoomModule {}
