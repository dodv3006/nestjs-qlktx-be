import { IsIn, IsInt, IsPositive } from 'class-validator';
import { StudentEntity } from 'src/student/entities/student.entity';
import {
  Entity,
  Column,
  OneToMany,
  PrimaryGeneratedColumn,
  BaseEntity,
} from 'typeorm';
import { ERoomType } from '../types';

@Entity('room')
export class RoomEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: number;

  @Column({ unique: true, nullable: false })
  name: string;

  @Column({
    type: 'enum',
    enum: ERoomType,
    default: ERoomType.MEDIUM,
  })
  @IsIn(Object.values(ERoomType))
  roomType: ERoomType;

  @Column()
  @IsPositive()
  price: number;

  @Column()
  @IsPositive()
  @IsInt()
  maxPeople: number;

  @OneToMany(() => StudentEntity, (student) => student.room, { lazy: true })
  students: StudentEntity[];
}
