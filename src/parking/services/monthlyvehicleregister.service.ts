import { Injectable } from '@nestjs/common';
import { BaseService } from 'src/common/services/BaseService';
import { MonthlyVehicleRegisterEntity } from '../entities/monthlyvehicleregister.entity';
import { VehicleEntity } from '../entities/vehicle.entity';
import { MonthlyVehicleRegisterRepository } from '../repositories/monthlyvehicleregister.repository';
import { VehicleRepository } from '../repositories/vehicle.repository';

@Injectable()
export class MonthlyVehicleRegisterService extends BaseService<
  MonthlyVehicleRegisterEntity,
  MonthlyVehicleRegisterRepository
> {
  constructor(repository: MonthlyVehicleRegisterRepository) {
    super(repository);
  }
}
