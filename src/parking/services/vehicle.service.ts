import { Injectable } from '@nestjs/common';
import { BaseService } from 'src/common/services/BaseService';
import { VehicleEntity } from '../entities/vehicle.entity';
import { VehicleRepository } from '../repositories/vehicle.repository';

@Injectable()
export class VehicleService extends BaseService<
  VehicleEntity,
  VehicleRepository
> {
  constructor(repository: VehicleRepository) {
    super(repository);
  }
}
