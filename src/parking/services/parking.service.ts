import { Injectable } from '@nestjs/common';
import { BaseService } from 'src/common/services/BaseService';
import { ParkingEntity } from '../entities/parking.entity';
import { ParkingRepository } from '../repositories/parking.repository';

@Injectable()
export class ParkingService extends BaseService<
  ParkingEntity,
  ParkingRepository
> {
  constructor(repository: ParkingRepository) {
    super(repository);
  }
}
