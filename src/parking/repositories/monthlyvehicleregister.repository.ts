import { EntityRepository, Repository } from 'typeorm';
import { MonthlyVehicleRegisterEntity } from '../entities/monthlyvehicleregister.entity';

@EntityRepository(MonthlyVehicleRegisterEntity)
export class MonthlyVehicleRegisterRepository extends Repository<MonthlyVehicleRegisterEntity> {}
