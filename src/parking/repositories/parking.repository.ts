import { EntityRepository, Repository } from 'typeorm';
import { ParkingEntity } from '../entities/parking.entity';
@EntityRepository(ParkingEntity)
export class ParkingRepository extends Repository<ParkingEntity> {}
