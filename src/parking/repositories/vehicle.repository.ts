import { EntityRepository, Repository } from 'typeorm';
import { VehicleEntity } from '../entities/vehicle.entity';

@EntityRepository(VehicleEntity)
export class VehicleRepository extends Repository<VehicleEntity> {}
