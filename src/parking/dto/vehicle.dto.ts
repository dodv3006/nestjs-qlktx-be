import { Exclude, Expose, Transform } from 'class-transformer';
import { IsNumber, IsString } from 'class-validator';
import { StudentDTO } from 'src/student/dto';
@Exclude()
export class VehicleDTO {
  @Expose()
  @IsString()
  readonly licensePlate: string;

  @Transform(({ value }) => value.name)
  readonly name: StudentDTO;

  constructor(partial: Partial<VehicleDTO>) {
    Object.assign(this, partial);
  }
}
