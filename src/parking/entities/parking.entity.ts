import { StudentEntity } from 'src/student/entities/student.entity';
import {
  Entity,
  BaseEntity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
} from 'typeorm';
import { MonthlyVehicleRegisterEntity } from './monthlyvehicleregister.entity';
import { VehicleEntity } from './vehicle.entity';

@Entity('parking')
export class ParkingEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'timestamp', nullable: true })
  checkIn: Date;

  @Column({ type: 'timestamp', nullable: true })
  checkOut: Date;

  @Column({ default: 0 })
  price: number;

  @ManyToOne(() => VehicleEntity, (vehicle) => vehicle.parkings)
  @JoinColumn({ name: 'vehicle_licenseplate' })
  vehicle: VehicleEntity;
}
