import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { VehicleEntity } from './vehicle.entity';

@Entity('monthlyvehicleregister')
export class MonthlyVehicleRegisterEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ type: 'date' })
  time: string;
  @ManyToOne(() => VehicleEntity, (vehicle) => vehicle.monthlyvehicleregister)
  @JoinColumn({ name: 'vehicle_lisenceplate' })
  vehicle: VehicleEntity;
}
