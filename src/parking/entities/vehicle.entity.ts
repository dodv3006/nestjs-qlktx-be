import { StudentEntity } from 'src/student/entities/student.entity';
import {
  Entity,
  PrimaryColumn,
  BaseEntity,
  ManyToOne,
  OneToMany,
  JoinColumn,
} from 'typeorm';
import { MonthlyVehicleRegisterEntity } from './monthlyvehicleregister.entity';
import { ParkingEntity } from './parking.entity';

@Entity('vehicle')
export class VehicleEntity extends BaseEntity {
  @PrimaryColumn()
  licensePlate: string;

  @ManyToOne(() => StudentEntity, (student) => student.vehicles)
  @JoinColumn({ name: 'student_id' })
  student: StudentEntity;

  @OneToMany(
    () => MonthlyVehicleRegisterEntity,
    (monthlyvehicleregister) => monthlyvehicleregister.vehicle,
    { lazy: true }
  )
  monthlyvehicleregister: MonthlyVehicleRegisterEntity[];

  @OneToMany(() => ParkingEntity, (parking) => parking.vehicle, { lazy: true })
  parkings: ParkingEntity[];
}
