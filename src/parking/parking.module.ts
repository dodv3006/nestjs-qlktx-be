import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ParkingController } from './controller/parking.controller';
import { MonthlyVehicleRegisterEntity } from './entities/monthlyvehicleregister.entity';
import { ParkingEntity } from './entities/parking.entity';
import { VehicleEntity } from './entities/vehicle.entity';
import { MonthlyVehicleRegisterRepository } from './repositories/monthlyvehicleregister.repository';
import { ParkingRepository } from './repositories/parking.repository';
import { VehicleRepository } from './repositories/vehicle.repository';
import { MonthlyVehicleRegisterService } from './services/monthlyvehicleregister.service';
import { ParkingService } from './services/parking.service';
import { VehicleService } from './services/vehicle.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ParkingEntity,
      ParkingRepository,
      MonthlyVehicleRegisterEntity,
      MonthlyVehicleRegisterRepository,
      VehicleEntity,
      VehicleRepository,
    ]),
  ],
  controllers: [ParkingController],
  providers: [VehicleService, ParkingService, MonthlyVehicleRegisterService],
  exports: [
    TypeOrmModule,
    VehicleService,
    ParkingService,
    MonthlyVehicleRegisterService,
  ],
})
export class ParkingModule {}
