import { Controller, Get, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { ManualSerialize } from 'src/common/interceptors/serialize.interceptor';
import { VehicleDTO } from '../dto/vehicle.dto';
import { VehicleService } from '../services/vehicle.service';
@Controller('parking')
@ApiTags('Parking')
@UseGuards(JwtAuthGuard)
export class ParkingController {
  constructor(private readonly vehicleService: VehicleService) {}

  @Get('get-all-vehicles')
  @ManualSerialize(VehicleDTO)
  async getAllVehicles() {
    return this.vehicleService.getAll();
  }
}
