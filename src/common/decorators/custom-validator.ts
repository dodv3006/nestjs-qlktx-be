import { IsEnum, IsOptional } from 'class-validator';
type ValidationEnumOptions = {
  enum: Record<string, any>;
  required?: boolean;
};
export function IsValidEnum(opts: ValidationEnumOptions): PropertyDecorator {
  return function (target: any, propertyKey: string | symbol): void {
    IsEnum(opts.enum)(target, propertyKey);
    if (opts.required === false) IsOptional()(target, propertyKey);
  };
}
