export enum ERole {
  ADMIN = 'ADMIN',
  STUDENT = 'STUDENT',
}
export enum EAction {
  Manage = 'manage',
  Create = 'create',
  Read = 'read',
  Update = 'update',
  Delete = 'delete',
}
