import {MigrationInterface, QueryRunner} from "typeorm";

export class Test1643272861841 implements MigrationInterface {
    name = 'Test1643272861841'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "student" ADD "phoneNumber" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "student" DROP COLUMN "phoneNumber"`);
    }

}
