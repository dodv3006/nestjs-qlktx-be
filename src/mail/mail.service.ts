import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { UserEntity } from 'src/auth/entities/user.entity';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  async sendUserConfirmationRegister(user: UserEntity) {
    const url = `http://localhost:8080/user/confirm-register?userId=${user.id}&code=${user.confirmationCode}`;

    await this.mailerService.sendMail({
      to: user.email,
      subject: 'Confirm your Email!',
      template: 'confirmation',
      context: {
        name: user.email,
        url,
      },
    });
  }

  async sendUserResetRequestPassword(user: UserEntity) {
    await this.mailerService.sendMail({
      to: user.email,
      subject: 'Reset Password!',
      template: 'resetPassword',
      context: {
        name: user.email,
        code: user.confirmationCode,
      },
    });
  }

}