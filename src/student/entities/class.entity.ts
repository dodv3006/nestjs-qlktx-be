import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  BaseEntity,
} from 'typeorm';
import { StudentEntity } from './student.entity';
@Entity('class')
export class ClassEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  classID: string;

  @Column()
  name: string;

  @OneToMany(() => StudentEntity, (student) => student.class, { lazy: true })
  students: StudentEntity[];
}
