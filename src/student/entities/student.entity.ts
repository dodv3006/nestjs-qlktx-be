import { UserEntity } from 'src/auth/entities/user.entity';
import { VehicleEntity } from 'src/parking/entities/vehicle.entity';
import { RoomEntity } from 'src/room/entities/room.entity';
import {
  Entity,
  Column,
  PrimaryColumn,
  ManyToOne,
  OneToMany,
  JoinColumn,
  BaseEntity,
  OneToOne,
} from 'typeorm';
import { ClassEntity } from './class.entity';
@Entity('student')
export class StudentEntity extends BaseEntity {
  @PrimaryColumn()
  studentID: string;

  @Column({ type: 'varchar', length: 100 })
  name: string;

  @Column()
  address: string;

  @Column({ type: 'date' })
  dateOfBirth: string;

  @Column()
  phoneNumber: string;

  @ManyToOne(() => ClassEntity, (belongedClass) => belongedClass.students, {
    eager: true,
    cascade: ['insert', 'update'],
  })
  @JoinColumn({ name: 'classId' })
  class: ClassEntity;

  @OneToMany(() => VehicleEntity, (vehicle) => vehicle.student, {
    eager: true,
    cascade: ['insert', 'update'],
  })
  vehicles: VehicleEntity[];

  @ManyToOne(() => RoomEntity, (room) => room.students, {
    eager: true,
    cascade: ['insert', 'update'],
  })
  @JoinColumn({ name: 'roomId' })
  room: RoomEntity;

  @OneToOne(() => UserEntity, (user) => user.student, { cascade: ['remove'] })
  user: UserEntity;
}
