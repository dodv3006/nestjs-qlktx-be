import { EntityRepository, Repository } from 'typeorm';
import { ClassEntity } from '../entities/class.entity';
@EntityRepository(ClassEntity)
export class ClassRepository extends Repository<ClassEntity> {}
