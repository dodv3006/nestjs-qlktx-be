import { Controller, Get, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { ManualSerialize } from 'src/common/interceptors/serialize.interceptor';
import { ClassDTO } from '../dto';
import { ClassService } from '../services/class.service';

@Controller('class')
@UseGuards(JwtAuthGuard)
export class ClassController {
  constructor(private readonly classService: ClassService) {}
  @Get('get-all-classes')
  @ManualSerialize(ClassDTO)
  getAllVisitors() {
    return this.classService.getAll();
  }
}
