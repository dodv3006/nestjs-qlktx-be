import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { PoliciesGuard } from 'src/auth/guards/policies.guard';
import { AppAbility } from 'src/casl/casl-ability.factory';
import { EAction, ERole } from 'src/common/constants/global.constant';
import { CheckPolicies } from 'src/common/decorators/policies.decorator';
import { Roles } from 'src/common/decorators/roles.decorator';
import { ManualSerialize } from 'src/common/interceptors/serialize.interceptor';
import { RoomService } from 'src/room/services/room.service';
import { StudentDTO } from '../dto';
import { CreateStudentDTO } from '../dto/create-student.dto';
import { StudentEntity } from '../entities/student.entity';
import { ClassService } from '../services/class.service';
import { StudentService } from '../services/student.service';

@Controller('student')
@ApiTags('Student')
@UseGuards(JwtAuthGuard, PoliciesGuard)
@ApiBearerAuth()
export class StudentController {
  constructor(
    private readonly studentService: StudentService,
    private readonly classService: ClassService,
    private readonly roomService: RoomService
  ) {}

  @Get('get-all-students')
  @ManualSerialize(StudentDTO)
  @CheckPolicies((ability: AppAbility) =>
    ability.can(EAction.Read, StudentEntity)
  )
  getAllStudents() {
    return this.studentService.getAllStudents();
  }

  @Post('/create-student')
  @ApiBody({ type: CreateStudentDTO })
  createStudent(@Body() createStudent: CreateStudentDTO) {
    return this.studentService.createStudent(createStudent);
  }
}
