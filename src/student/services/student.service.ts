import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { RoomRepository } from 'src/room/repositories/room.repository';
import { CreateStudentDTO } from '../dto/create-student.dto';
import { StudentEntity } from '../entities/student.entity';
import { ClassRepository } from '../repositories/class.repository';
import { StudentRepository } from '../repositories/student.repository';

@Injectable()
export class StudentService {
  constructor(
    private readonly studentRepository: StudentRepository,
    private readonly classRepository: ClassRepository,
    private readonly roomRepository: RoomRepository
  ) {}
  getAllStudents() {
    return this.studentRepository.find();
  }

  async createStudent(createStudent: CreateStudentDTO) {
   const { studentID, name, address, dateOfBirth } = createStudent;
   console.log(
     '🚀 ~ file: student.service.ts ~ line 21 ~ StudentService ~ createStudent ~ createStudent',
     createStudent
   );
   const belongedClass = await this.classRepository.findOne(
     createStudent.classID
   );
   const belongedRoom = await this.roomRepository.findOne(createStudent.roomID);

   if (!belongedClass) {
     throw new InternalServerErrorException(
       `Can't now find the class with id: ${createStudent.classID}`
     );
   }
   if (!belongedRoom) {
     throw new InternalServerErrorException(
       `Can't now find the room with id: ${createStudent.roomID}`
     );
   }
   return this.studentRepository.save({
     studentID: studentID,
     name: name,
     address: address,
     dateOfBirth: dateOfBirth,
     class: belongedClass,
     room: belongedRoom,
   });
  }
}
