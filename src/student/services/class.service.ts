import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from 'src/common/services/BaseService';
import { Repository } from 'typeorm';
import { ClassEntity } from '../entities/class.entity';
import { ClassRepository } from '../repositories/class.repository';

@Injectable()
export class ClassService extends BaseService<ClassEntity, ClassRepository> {
  constructor(repository: ClassRepository) {
    super(repository);
  }
}
