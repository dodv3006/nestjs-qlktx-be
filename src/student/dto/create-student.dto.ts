import { Exclude, Expose } from 'class-transformer';
import { IsDateString, IsInt, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

@Exclude()
export class CreateStudentDTO {
  @Expose()
  @IsString()
  @ApiProperty({ required: true })
  studentID: string;

  @Expose()
  @IsString()
  @ApiProperty()
  name: string;

  @Expose()
  @ApiProperty()
  @IsString()
  address: string;

  @Expose()
  @ApiProperty()
  @IsDateString()
  dateOfBirth: string;

  @Expose()
  @ApiProperty()
  @IsInt()
  classID: number;

  @Expose()
  @ApiProperty()
  @IsInt()
  roomID: number;

  constructor(partial: Partial<CreateStudentDTO>) {
    Object.assign(this, partial);
  }
}
