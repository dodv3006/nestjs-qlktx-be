import { Exclude, Expose } from 'class-transformer';
import { IsNumber, IsString } from 'class-validator';
@Exclude()
export class ClassDTO {
  @Expose()
  @IsNumber()
  readonly classID: number;

  @Expose()
  @IsString()
  readonly name: string;

  constructor(partial: Partial<ClassDTO>) {
    Object.assign(this, partial);
  }
}
