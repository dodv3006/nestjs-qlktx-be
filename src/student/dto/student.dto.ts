import { Exclude, Expose, Transform, Type } from 'class-transformer';
import { IsDateString, IsString, ValidateNested } from 'class-validator';
import { RoomDTO } from 'src/room/dto/room.dto';
import { ClassDTO } from '.';
@Exclude()
export class StudentDTO {
  @Expose()
  @IsString()
  readonly studentID: string;

  @Expose()
  @IsString()
  readonly name: string;

  @Expose()
  @IsString()
  readonly address: string;

  @Expose()
  @IsDateString()
  readonly dateOfBirth: string;

  @Expose({ name: 'class' })
  @Transform(({ value }: { value: ClassDTO }) => value.name)
  className: string;

  @Expose({ name: 'room' })
  @Transform(({ value }: { value: RoomDTO }) => value.name)
  roomName: string;

  constructor(partial: Partial<StudentDTO>) {
    Object.assign(this, partial);
  }
}
