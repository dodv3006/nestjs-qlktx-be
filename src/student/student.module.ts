import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CaslModule } from 'src/casl/casl.module';
import { RoomModule } from 'src/room/room.module';
import { ClassController } from './controller/class.controller';
import { StudentController } from './controller/student.controller';
import { ClassEntity } from './entities/class.entity';
import { StudentEntity } from './entities/student.entity';
import { ClassRepository } from './repositories/class.repository';
import { StudentRepository } from './repositories/student.repository';
import { ClassService } from './services/class.service';
import { StudentService } from './services/student.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      StudentEntity,
      ClassEntity,
      StudentRepository,
      ClassRepository,
    ]),
    RoomModule,
    CaslModule,
  ],
  controllers: [StudentController, ClassController],
  providers: [ClassService, StudentService],
  exports: [TypeOrmModule, ClassService, StudentService],
})
export class StudentModule {}
