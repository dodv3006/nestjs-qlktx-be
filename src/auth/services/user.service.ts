import {
  ForbiddenException,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { LoginDTO } from '../dto/login.dto';
import { RegisterDTO } from '../dto/register.dto';
import { UserRepository } from '../repositories/user.repository';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { isEmail } from 'class-validator';
import { UserEntity } from '../entities/user.entity';
import { StudentRepository } from 'src/student/repositories/student.repository';
import { MailService } from 'src/mail/mail.service';
import { randomUUID } from 'crypto';
import { ConfirmResetPasswordDTO } from '../dto/confirmResetPassword.dto';

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly jwtService: JwtService,
    private readonly studentRepository: StudentRepository,
    private readonly mailService : MailService
  ) {}

  async login(body: LoginDTO) {
    const userDetails = await this.userRepository.findOne(
        isEmail(body.usernameOrEmail) ? {email: body.usernameOrEmail} : {username: body.usernameOrEmail}
      )

    if (!userDetails) {
      throw new HttpException(
        `Can not found user with: ${body.usernameOrEmail}`,
        HttpStatus.EXPECTATION_FAILED
      );
    }

    if(!userDetails.activated) {
      throw new HttpException(`Account '${body.usernameOrEmail}' not activated`, HttpStatus.EXPECTATION_FAILED)
    }

    const isValid = bcrypt.compareSync(
      body.password,
      userDetails.encryptedPassword
    );
    if (isValid) {
      return {
        accessToken: this.jwtService.sign({
          usernameOrEmail: body.usernameOrEmail,
        }),
      };
    } else {
      throw new ForbiddenException('Invalid Credentials');
    }
  }

  async register(body: RegisterDTO) {
    const student = await this.studentRepository.findOne(body.studentID);
    if (!student) {
      throw new HttpException({
        status: HttpStatus.EXPECTATION_FAILED,
        error: `Can't not find student with ID: ${body.studentID}`,
      }, HttpStatus.EXPECTATION_FAILED);
    }
    const user = new UserEntity();
    if (isEmail(body.usernameOrEmail)) {
      user.email = body.usernameOrEmail;
    } else {
      user.username = body.usernameOrEmail;
    }
    user.id = randomUUID();
    user.student = student;
    user.encryptedPassword = bcrypt.hashSync(body.password, 10);
    user.confirmationCode = Math.floor(100000 + Math.random() * 900000);
    user.codeExp = new Date();

    this.userRepository.save(user);

    this.mailService.sendUserConfirmationRegister(user)
  }

  async confirmRegister(userId : string, code : number) {
    const user = await this.userRepository.findOne( {id: userId} );

    if(user.codeExp == null && user.confirmationCode == null) {
      throw new HttpException({
        status: HttpStatus.EXPECTATION_FAILED,
        error: `Your account has no request to change`,
      }, HttpStatus.EXPECTATION_FAILED);
    } 

    if(user.codeExp.getSeconds() > Date.now()) {
      throw new HttpException({
        status: HttpStatus.EXPECTATION_FAILED,
        error: `Code has expired`,
      }, HttpStatus.EXPECTATION_FAILED);
    }

    if(user.confirmationCode != code) {
      throw new HttpException({
        status: HttpStatus.EXPECTATION_FAILED,
        error: `The code is incorrect`,
      }, HttpStatus.EXPECTATION_FAILED);
    }
    
    // this.checkEmailConfirmValid(user, code);

    user.confirmationCode = null;
    user.codeExp = null;
    user.activated = true;

    this.userRepository.save(user);
    return true;
  }

  async requestResetPassword(studentId : string, email : string) {
    let student = await this.studentRepository.findOne({studentID: studentId});

    let user = await this.userRepository.findOne( {student: student, email: email} );

    user.confirmationCode = Math.floor(100000 + Math.random() * 900000);
    user.codeExp = new Date();

    this.userRepository.save(user);

    this.mailService.sendUserResetRequestPassword(user);

    return {
      studentId,
      email,
      codeExp: user.codeExp
    }
  }

  async resetPassword(dto : ConfirmResetPasswordDTO) {
    if(dto.newPassword != dto.confirmPassword) {
      throw new HttpException({
        status: HttpStatus.EXPECTATION_FAILED,
        error: `Password and Confirm password must match`,
      }, HttpStatus.EXPECTATION_FAILED);
    }
    
    let student = await this.studentRepository.findOne({studentID: dto.studentID});
    if(student == null) {
      throw new HttpException({
        status: HttpStatus.EXPECTATION_FAILED,
        error: `Can't not find student with Id= ` + dto.studentID,
      }, HttpStatus.EXPECTATION_FAILED);
    }

    let user = await this.userRepository.findOne( {student: student} );

    if(user.codeExp == null && user.confirmationCode == null) {
      throw new HttpException({
        status: HttpStatus.EXPECTATION_FAILED,
        error: `Your account has no request to change`,
      }, HttpStatus.EXPECTATION_FAILED);
    } 

    if(user.codeExp.getSeconds() > Date.now()) {
      throw new HttpException({
        status: HttpStatus.EXPECTATION_FAILED,
        error: `Code has expired`,
      }, HttpStatus.EXPECTATION_FAILED);
    }

    if(user.confirmationCode != dto.code) {
      throw new HttpException({
        status: HttpStatus.EXPECTATION_FAILED,
        error: `The code is incorrect`,
      }, HttpStatus.EXPECTATION_FAILED);
    }

    // this.checkEmailConfirmValid(user, dto.code);

    user.encryptedPassword = user.encryptedPassword = bcrypt.hashSync(dto.newPassword, 10);
    user.codeExp = null;
    user.confirmationCode = null;

    this.userRepository.save(user);
    return true;
  }

  async checkEmailConfirmValid(user : UserEntity, code : number) { // why not work
    if(user.codeExp == null && user.confirmationCode == null) {
      throw new HttpException({
        status: HttpStatus.EXPECTATION_FAILED,
        error: `Your account has no request to change`,
      }, HttpStatus.EXPECTATION_FAILED);
    } 

    if(user.codeExp.getSeconds() > Date.now()) {
      throw new HttpException({
        status: HttpStatus.EXPECTATION_FAILED,
        error: `Code has expired`,
      }, HttpStatus.EXPECTATION_FAILED);
    }

    if(user.confirmationCode != code) {
      throw new HttpException({
        status: HttpStatus.EXPECTATION_FAILED,
        error: `The code is incorrect`,
      }, HttpStatus.EXPECTATION_FAILED);
    }
  }

  async getActiveUser(usernameOrEmail: string) {
    let user = null;

    if(isEmail(usernameOrEmail)) {
      user = await this.userRepository.findOne({email: usernameOrEmail});

      return user;
    }

    user = await this.userRepository.findOne({ username: usernameOrEmail });

    return user;
  }
}
