import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailService } from 'src/mail/mail.service';
import { StudentModule } from 'src/student/student.module';
import { jwtConstants } from './constant';
import { AuthController } from './controller/auth.controller';
import { UserEntity } from './entities/user.entity';
import { RolesGuard } from './guards/roles.guard';
import { UserRepository } from './repositories/user.repository';
import { UserService } from './services/user.service';
import { JwtStrategy } from './strategy/jwt.strategy';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity, UserRepository]),
    StudentModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return {
          secret: jwtConstants.secret,
          signOptions: { expiresIn: 60 * 60 * 24, algorithm: 'HS256' },
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [UserService, JwtStrategy, RolesGuard, MailService],
  controllers: [AuthController],
  exports: [TypeOrmModule, UserService],
})
export class AuthModule {}
