import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import { jwtConstants } from '../constant';
import { UserService } from '../services/user.service';
dotenv.config();
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
      algorithms: ['HS256'],
    });
  }

  async validate(payload: any) {
    const { usernameOrEmail } = payload;
    const user = await this.userService.getActiveUser(usernameOrEmail);
    return user;
  }
}
