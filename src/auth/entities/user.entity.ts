import { IsEmail, IsIn } from 'class-validator';
import { ERole } from 'src/common/constants/global.constant';
import { StudentEntity } from 'src/student/entities/student.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  OneToOne,
  JoinColumn,
} from 'typeorm';

@Entity('user')
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true, nullable: true })
  username: string;

  @Column({ unique: true, nullable: true })
  @IsEmail()
  email: string;

  @Column({ nullable: false, name: 'encrypted_password' })
  encryptedPassword: string;

  @Column({ type: 'enum', enum: ERole, default: ERole.STUDENT })
  role: ERole;

  @OneToOne(() => StudentEntity, (student) => student.user, {
    cascade: ['insert', 'update'],
    eager: true,
  })
  @JoinColumn({ name: 'studentId' })
  student: StudentEntity;

  @Column({nullable: true})
  confirmationCode : number;

  @Column({type: 'timestamptz', nullable: true})
  codeExp : Date;

  @Column({type: 'boolean', default : false})
  activated : boolean;
  
}
