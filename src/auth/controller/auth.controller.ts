import { Body, Controller, Get, Param, Post, Req, Res, UseGuards } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { LoginDTO } from '../dto/login.dto';
import { RegisterDTO } from '../dto/register.dto';
import { UserService } from '../services/user.service';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';
import { RequestRestPasswordDTO } from '../dto/requestResetPassword.dto';
import { ConfirmResetPasswordDTO } from '../dto/confirmResetPassword.dto';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(
    private readonly userService: UserService
  ) {}

  @Post('login')
  @ApiBody({ type: LoginDTO })
  async login(@Body() body: LoginDTO) {
    return this.userService.login(body);
  }

  @Post('register')
  @ApiBody({ type: RegisterDTO })
  async register(@Body() body: RegisterDTO) {
    return this.userService.register(body);
  }

  @Get("confirm-register/userId=:userId&code=:code")
  async confirmRegister(@Param('userId') userId : string, @Param('code') code : number) {
      return this.userService.confirmRegister(userId, code);
  }

  @Post("request-reset-password")
  @ApiBody({type: RequestRestPasswordDTO})
  async requestResetPassword(@Body() dto : RequestRestPasswordDTO) {
    return this.userService.requestResetPassword(dto.studentID, dto.email);
  }

  @Post("reset-password")
  @ApiBody({type: ConfirmResetPasswordDTO})
  async resetPassword(@Body() dto : ConfirmResetPasswordDTO) {
    return this.userService.resetPassword(dto);
  }
}
