import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform } from 'class-transformer';
import { IsNumber, IsString, MinLength } from 'class-validator';
import { StudentDTO } from 'src/student/dto';
@Exclude()
export class LoginDTO {
  @Expose()
  @IsString()
  @MinLength(6)
  @ApiProperty()
  readonly usernameOrEmail: string;

  @Expose()
  @IsString()
  @MinLength(8)
  @ApiProperty()
  readonly password: string;

  constructor(partial: Partial<LoginDTO>) {
    Object.assign(this, partial);
  }
}
