import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsString, MinLength } from 'class-validator';
@Exclude()
export class RequestRestPasswordDTO {
  @Expose()
  @IsString()
  @ApiProperty()
  readonly studentID: string;

  @Expose()
  @IsString()
  @MinLength(6)
  @ApiProperty()
  readonly email: string;


  constructor(partial: Partial<RequestRestPasswordDTO>) {
    Object.assign(this, partial);
  }
}
