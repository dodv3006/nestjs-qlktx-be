import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IsNumber, IsString, MinLength } from 'class-validator';
@Exclude()
export class ConfirmResetPasswordDTO {
  @Expose()
  @IsString()
  @ApiProperty()
  readonly studentID: string;

  @Expose()
  @IsNumber()
  @ApiProperty()
  readonly code: number;

  @Expose()
  @IsString()
  @MinLength(6)
  @ApiProperty()
  readonly newPassword : string;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly confirmPassword : string;

  constructor(partial: Partial<ConfirmResetPasswordDTO>) {
    Object.assign(this, partial);
  }
}
