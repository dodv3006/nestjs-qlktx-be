import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform } from 'class-transformer';
import { IsEmail, IsOptional, IsString, MinLength } from 'class-validator';
@Exclude()
export class RegisterDTO {
  @Expose()
  @IsString()
  @ApiProperty()
  readonly studentID: string;

  @Expose()
  @IsString()
  @MinLength(6)
  @ApiProperty()
  readonly usernameOrEmail: string;

  @Expose()
  @IsString()
  @MinLength(8)
  @ApiProperty()
  readonly password: string;

  constructor(partial: Partial<RegisterDTO>) {
    Object.assign(this, partial);
  }
}
