import { Injectable } from '@nestjs/common';
import { UserService } from './auth/services/user.service';

@Injectable()
export class AppService {

  getHello(): string {
    return 'Hello World!';
  }
}
