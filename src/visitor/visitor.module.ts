import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VisitorEntity } from './entities/visitor.entity';
import { VisitorRepository } from './entities/vistor.repository';
import { VisitorController } from './visitor.controller';
import { VisitorService } from './visitor.service';

@Module({
  imports: [TypeOrmModule.forFeature([VisitorEntity, VisitorRepository])],
  controllers: [VisitorController],
  providers: [VisitorService],
  exports: [TypeOrmModule, VisitorService],
})

export class VisitorModule {}
