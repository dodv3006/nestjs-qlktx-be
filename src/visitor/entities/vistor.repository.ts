import { EntityRepository, Repository } from 'typeorm'
import { VisitorEntity } from './visitor.entity'

@EntityRepository(VisitorEntity)
export class VisitorRepository extends Repository<VisitorEntity> {
  
}