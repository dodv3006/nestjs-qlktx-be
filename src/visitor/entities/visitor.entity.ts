import { Entity, Column, PrimaryColumn, BaseEntity } from 'typeorm';

@Entity('visitor')
export class VisitorEntity extends BaseEntity {
  @PrimaryColumn({ type: 'character', length: 12 })
  vistorIdCard: string;

  @Column({ type: 'varchar', length: 100 })
  name: string;
}
