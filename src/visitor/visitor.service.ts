import { Injectable } from '@nestjs/common';
import { BaseService } from 'src/common/services/BaseService';
import { VisitorEntity } from './entities/visitor.entity';
import { VisitorRepository } from './entities/vistor.repository';

@Injectable()
export class VisitorService extends BaseService<
  VisitorEntity,
  VisitorRepository
> {
  constructor(repository: VisitorRepository) {
    super(repository);
  }

  async getAllVisitors(): Promise<VisitorEntity[]> {
    const res = await this.getAll();
    return res;
  }
}
