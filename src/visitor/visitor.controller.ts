import { Controller, Get } from '@nestjs/common';
import { ManualSerialize } from 'src/common/interceptors/serialize.interceptor';
import { VisitorDTO } from './dto';
import { VisitorService } from './visitor.service';

@Controller('visitor')
export class VisitorController {

  constructor(private readonly visitorService: VisitorService) {}

  @Get('get-all-visitors')
  @ManualSerialize(VisitorDTO)
  getAllVisitors() {
    return this.visitorService.getAllVisitors();
  }

}
