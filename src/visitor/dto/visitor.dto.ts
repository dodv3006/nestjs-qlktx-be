import { Exclude, Expose } from 'class-transformer';
import { IsString } from 'class-validator';
@Exclude()
export class VisitorDTO {
  @Expose()
  @IsString()
  readonly vistorIdCard: string;

  @Expose()
  @IsString()
  readonly name: string;

  constructor(partial: Partial<VisitorDTO>) {
    Object.assign(this, partial);
  }
}
