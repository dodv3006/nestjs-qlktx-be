import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
const config: PostgresConnectionOptions = {
  type: 'postgres',
  database: process.env.POSTGRES_DB,
  host: process.env.POSTGRES_HOST,
  port: +process.env.POSTGRES_PORT,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  migrationsRun: true,
  synchronize: false,
  migrations: ['dist/src/migrations/*.js'],
  entities: ['dist/**/*.entity.js'],
  cli: {
    migrationsDir: 'src/migrations',
  },
  migrationsTableName: 'magration_table',
};
export default config;